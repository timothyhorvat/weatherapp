﻿using UnityEngine;

public static class ParticleSystemExtensions
{
    public static void StartParticles(this ParticleSystem particles)
    {
        if (particles.isEmitting)
        {
            return;
        }
        particles.Play();
    }

    public static void StopParticles(this ParticleSystem particles)
    {
        if (particles.isStopped)
        {
            return;
        }
        particles.Clear();
        particles.Stop();
    }
}
