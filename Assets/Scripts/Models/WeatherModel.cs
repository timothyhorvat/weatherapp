﻿using System;
using System.Collections.Generic;

[Serializable]
public class WeatherModel
{
    public int id;
    public string name;
    public List<Weather> weather;
    public Main main;
}

[Serializable]
public class Weather
{
    public int id;
    public string main;
}

[Serializable]
public class Main
{
    public float temp;
}
