﻿using System;

[Serializable]
public class WeatherDto
{
    public string Name;
    public float Temperature;
    public string Condition;
}