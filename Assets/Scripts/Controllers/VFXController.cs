﻿using UnityEngine;

public class VFXController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem clouds;
    [SerializeField]
    private ParticleSystem rain;
    [SerializeField]
    private ParticleSystem snow;
    [SerializeField]
    private ParticleSystem sunshafts;

    public void EnableCloudsVFX()
    {
        DisableAllVFX();
        EnableClouds();
    }

    public void EnableRainVFX()
    {
        DisableAllVFX();
        EnableClouds();
        EnableRain();
    }

    public void EnableSnowVFX()
    {
        DisableAllVFX();
        EnableClouds();
        EnableSnow();
    }

    public void EnableClearVFX()
    {
        DisableAllVFX();
        EnableSunshafts();
    }

    private void DisableAllVFX()
    {
        DisableClouds();
        DisableRain();
        DisableSnow();
        DisableSunshafts();
    }


    #region Particle System Controls

    private void EnableClouds()
    {
        clouds.StartParticles();
    }

    private void DisableClouds()
    {
        clouds.StopParticles();
    }

    private void EnableRain()
    {
        rain.StartParticles();
    }

    private void DisableRain()
    {
        rain.StopParticles();
    }

    private void EnableSnow()
    {
        snow.StartParticles();
    }

    private void DisableSnow()
    {
        snow.StopParticles();
    }

    private void EnableSunshafts()
    {
        sunshafts.StartParticles();
    }

    private void DisableSunshafts()
    {
        sunshafts.StopParticles();
    }

    #endregion
}