﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Data;
using WeatherApp.Services;

namespace WeatherApp.Controllers
{
    public class UIController : MonoBehaviour
    {
        public Text NameText;
        public Text TempText;
        public Text CondText;

        public Dropdown LocationDropdown;

        private List<string> locations;

        private WeatherDto currentWeather;
        private WeatherService service;

        [SerializeField]
        private VFXController vfx;

        void Start()
        {
            InitialiseVariables();
            AssignDropdownOptions();
            AddListeners();
            UpdateCurrentLocation(LocationDropdown.value);
        }

        private void InitialiseVariables()
        {
            service = new WeatherService();
            locations = Locations.GetAvailableLocations();
        }

        private void AssignDropdownOptions()
        {
            List<Dropdown.OptionData> dropdownOptions = new List<Dropdown.OptionData>();

            for (int i = 0; i < locations.Count; i++)
            {
                Dropdown.OptionData data = new Dropdown.OptionData(locations[i]);
                dropdownOptions.Add(data);
            }
            LocationDropdown.options = dropdownOptions;
        }

        private void AddListeners()
        {
            LocationDropdown.onValueChanged.AddListener(UpdateCurrentLocation);
        }

        private void UpdateCurrentLocation(int value)
        {
            string newLocation = locations[value];
            UpdateWeatherDetails(newLocation);
        }

        private void UpdateWeatherDetails(string newLocation)
        {
            UpdateCurrentWeather(newLocation);
            UpdateWeatherUI();
            UpdateVFX();
        }

        private void UpdateCurrentWeather(string newLocation)
        {
            currentWeather = service.GetCurrentWeather(newLocation);
        }

        private void UpdateWeatherUI()
        {
            NameText.text = currentWeather.Name;
            TempText.text = currentWeather.Temperature.ToString() + "°";
            CondText.text = currentWeather.Condition;
        }

        private void UpdateVFX()
        {
            switch (currentWeather.Condition)
            {
                case "Thunderstorm":
                case "Drizzle":
                case "Rain":
                    vfx.EnableRainVFX();
                    break;
                case "Snow":
                    vfx.EnableSnowVFX();
                    break;
                case "Clear":
                    vfx.EnableClearVFX();
                    break;
                case "Clouds":
                    vfx.EnableCloudsVFX();
                    break;
            }
        }

    }
}
