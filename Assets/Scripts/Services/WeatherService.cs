﻿using System.IO;
using System.Net;
using UnityEngine;

namespace WeatherApp.Services
{
    public class WeatherService
    {
        private const string KEY = "9ead6b07b2aaf4b470cf09ac9c205ac8";
        private const string UNIT = "metric";

        public WeatherDto GetCurrentWeather(string location)
        {
            HttpWebRequest request =
                (HttpWebRequest)WebRequest.Create(string.Format("http://api.openweathermap.org/data/2.5/weather?q={0}&units={1}&appid={2}",
                location.ToString(), UNIT, KEY));

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());

            string jsonResponse = reader.ReadToEnd();

            WeatherModel model = JsonUtility.FromJson<WeatherModel>(jsonResponse);

            WeatherDto weather = new WeatherDto
            {
                Name = model.name,
                Temperature = model.main.temp,
                Condition = model.weather[0].main
            };

            return weather;
        }
    }
}
