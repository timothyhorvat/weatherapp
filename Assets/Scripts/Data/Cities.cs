﻿using System.Collections.Generic;

namespace WeatherApp.Data
{
    public static class Locations
    {
        private static List<string> locations = new List<string>()
        {
            "London",
            "New York",
            "Moscow",
            "Sydney",
            "Tokyo",
            "Seville",
        };

        public static List<string> GetAvailableLocations()
        {
            return locations;
        }
    }
}